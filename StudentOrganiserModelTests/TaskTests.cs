﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using StudentOrganiserModel;

namespace StudentOrganiserModelTests
{
    [TestFixture]
    internal class TaskTests
    {
        [SetUp]
        public void SetUp()
        {
            _taskList = new TaskList();

            _sampleTaskItem1 = new TaskItem(
                "Vacuum Lounge area",
                "Need to fit new bag",
                TaskItem.TaskPriority.Low);

            _sampleTaskItem2 = new TaskItem(
                "Wash Dishes",
                "nice and clean",
                TaskItem.TaskPriority.Normal);

            _sampleTaskItem3 = new TaskItem(
                "Walk dog",
                "Take bags to pick Dishes up any whoopsies",
                TaskItem.TaskPriority.High);
        }

        private TaskList _taskList;
        private TaskItem _sampleTaskItem1, _sampleTaskItem2, _sampleTaskItem3;

        private void AddSampleItems()
        {
            _taskList.Add(_sampleTaskItem1);
            _taskList.Add(_sampleTaskItem2);
            _taskList.Add(_sampleTaskItem3); 
        }

        [Test]
        public void AddTaskItemToItemList()
        {
            int initialTaskListItemCount = _taskList.Count;
            _taskList.Add(_sampleTaskItem1);
            int finalTaskListItemCount = _taskList.Count;

            Assert.That(finalTaskListItemCount, Is.GreaterThan(initialTaskListItemCount));
        }

        [Test]
        public void CreateNewTaskItem()
        {
            var taskItem = new TaskItem(
                "The Title",
                "The Description",
                TaskItem.TaskPriority.Low);

            Assert.That(taskItem.Title, Is.EqualTo("The Title"));
        }

        [Test]
        public void CreateNewTaskItemWithDueDate()
        {
            var dateTime = new DateTime(2014, 2, 19, 16, 00, 00);

            var taskItem = new TaskItem(
                "The Title",
                "The Description",
                TaskItem.TaskPriority.Low,
                dateTime);

            Assert.That(taskItem.DueDate, Is.EqualTo(dateTime));
        }

        [Test]
        public void EmptyTaskList()
        {
            Assert.That(_taskList, Is.Not.Null);
        }

        [Test]
        public void SearchForItemsInItemList()
        {
            AddSampleItems();

            var results = _taskList["Dishes"];


            Assert.That(results.Count(), Is.GreaterThan(1));
        }

        [Test]
        public void SearchListByPriority()
        {
            AddSampleItems();
            var results = _taskList[TaskItem.TaskPriority.High];


            Assert.That(results.Count(), Is.GreaterThan(0));
        }
    }
}