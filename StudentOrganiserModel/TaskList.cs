﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace StudentOrganiserModel
{
    public class TaskList : ICollection<TaskItem>
    {
        private readonly List<TaskItem> _list;

        // constructor creating the empty list
        public TaskList()
        {
            _list = new List<TaskItem>();
        }

        // allows for searching title and description using indexer
        public IEnumerable<TaskItem> this[string searchString]
        {
            get
            {
                return _list.Where(p => p.Description.Contains(searchString)
                                        || p.Title.Contains(searchString));
            }
        }

        // returm item at a particular index
        public TaskItem this[int index]
        {
            get
            {
                if (index > -1 && index < _list.Count)
                {
                    return _list[index];
                }

                throw new IndexOutOfRangeException();
            }

            set { _list[index] = value; }
        }

        // return list of items matching priority
        public IEnumerable<TaskItem> this[TaskItem.TaskPriority priority]
        {
            get { return _list.Where(p => p.Priority == priority); }
        }

        // Add task item to list
        public void Add(TaskItem taskItem)
        {
            _list.Add(taskItem);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(TaskItem item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(TaskItem[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        bool ICollection<TaskItem>.Remove(TaskItem item)
        {
            ;
            return _list.Remove(item);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly { get; private set; }

        public IEnumerator<TaskItem> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public void Remove(TaskItem taskItem)
        {
            _list.Remove(taskItem);
        }
    }
}