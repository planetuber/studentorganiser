﻿using System;

namespace StudentOrganiserModel
{
    public class TaskItem
    {
        public enum TaskPriority
        {
            High,
            Normal,
            Low
        };

        public TaskItem(string title, string description, TaskPriority priority)
        {
            Title = title;
            Description = description;
            Priority = priority;
        }

        public TaskItem(string title, string description, TaskPriority priority, DateTime dueDate)
        {
            Title = title;
            Description = description;
            Priority = priority;
            DueDate = dueDate;
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateCompleted { get; set; }
        public TaskPriority Priority { get; set; }
    }
}